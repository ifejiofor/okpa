// Vehicle.h: Vehicle class definition.

#pragma once

#include "CoreMinimal.h"
#include "ActorThatCanMove.h"
#include "Vehicle.generated.h"


/**
 * DataAboutActorHeldInVehicle defines the data that a Vehicle stores about each actor it holds.
 * @author Ejiofor, Ifechukwu.
 */
USTRUCT()
struct FDataAboutActorHeldByVehicle
{
	GENERATED_BODY()

public:
	/** Pointer to the actor that is held by the vehicle. */
	UPROPERTY()
	AActor* Actor;

	/** Location of the held actor relative to the location of the vehicle. */
	UPROPERTY()
	FVector RelativeLocation;
};


/**
 * Vehicle defines an actor that can hold other actors, move, and cause the actors that it is holding to move in sync with it.
  * @author	Ejiofor Ifechukwu.
  * @see	AActorThatCanMove.
  */
UCLASS()
class OKPA_API AVehicle : public AActorThatCanMove
{
	GENERATED_BODY()
	
public:
	/** Sets default values for this vehicle's properties. */
	AVehicle();

	/** Sets the capacity of this vehicle.
		@note The capacity of this vehicle is the maximum number of actors that can be held by this vehicle. */
	void SetCapacity(int NewCapacity);

	/** Returns the capacity of this vehicle. */
	int GetCapacity() const;

	/** Returns the number of actors that are currently being held by this vehicle. */
	int GetNumberOfActorsThatAreBeingHeld() const;

	/** Causes this vehicle to hold a new actor. Once this vehicle holds an actor, any subsequent movement by this vehicle will cause that actor to move also.
		@return	True, if succeeded in holding the new actor. False, otherwise.
		@note	If this vehicle is full, it will not succeed in holding the new actor. */
	bool HoldActor(AActor* NewActor, const FVector& RelativeLocationWhereNewActorShouldBeHeld = FVector(0));

	/** Causes this vehicle to stop holding an actor that it is currently holding.
		@param	Actor	Actor that this vehicle should stop holding. If this parameter is nullptr, this vehicle will stop holding the most recently held actor.
		@return			If this vehicle successfully stopped holding an actor, returns a pointer to that actor, otherwise, returns nullptr. */
	AActor* StopHoldingActor(AActor* Actor = nullptr);

	/** Returns true if the specified actor is one of the actors that are being held by this vehicle. Returns false otherwise. */
	bool ActorIsBeingHeld(AActor* Actor) const;

	/** Returns true if this vehicle is full, i.e., is holding its maximum number of actors, otherwise returns false. */
	bool IsFull() const;

	/** Returns true if this vehicle is empty, i.e., not holding any actor, otherwise returns false. */
	bool IsEmpty() const;

	/** Performs the processing that needs to be performed periodically. */
	virtual void Tick(float DeltaTime) override;

	/** Moves this vehicle, and its held actors, instantly, to the specified location.
		@see AActor::SetActorLocation. */
	bool SetActorLocation(const FVector& NewLocation, bool bSweep = false, FHitResult* OutSweepHitResult = nullptr, ETeleportType Teleport = ETeleportType::None);

protected:
	/** Returns the index where data about the specified actor is stored within the VehicleSlots array. */
	int GetIndexOfVehicleSlotOccupiedByActor(const AActor* Actor) const;

private:
	/** Syncronizes the locations of all actors that are being held by this vehicle so that they align with the location of this vehicle. */
	void SyncronizeLocationsOfHeldActors();

private:
	/** Data about all the actors that are being held by this vehicle. */
	UPROPERTY()
	TArray<FDataAboutActorHeldByVehicle> VehicleSlots;
};