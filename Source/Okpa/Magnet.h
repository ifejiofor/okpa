// Magnet.h: Magnet class definition.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Classes/PaperSpriteComponent.h"
#include "Magnet.generated.h"


/**
 * Magnet defines an actor that attracts any okpa piece that comes close enough to it.
 * @author Ejiofor Ifechukwu.
 */
UCLASS()
class OKPA_API AMagnet : public AActor
{
	GENERATED_BODY()
	
public:	
	/** Sets default values for this magnet's properties */
	AMagnet();

	/** Sets the sprite that will be used to render this magnet. */
	void SetRenderSprite(UPaperSprite* Sprite);

	/** Sets the radius of the field of this magnet. */
	void SetRadiusOfField(float NewRadiusOfField);

	/** Sets the strength of the field of this magnet. */
	void SetStrengthOfField(float NewStrengthOfField);

	/** Returns the radius of the field of this magnet. */
	float GetRadiusOfField() const;

	/** Returns the strength of the field of this magnet. */
	float GetStrengthOfField() const;

	/** Performs the processing that is to be performed periodically. */
	virtual void Tick(float DeltaTime) override;

protected:
	/** Performs the processing that is to be performed when this magnet is spawned. */
	virtual void BeginPlay() override;

	/** Strength of the field of this magnet. */
	float StrengthOfField;

	/** Radius of the field of this magnet. */
	float RadiusOfField;

	/** Component that is used to render this magnet. */
	UPROPERTY(VisibleAnywhere)
	UPaperSpriteComponent* RenderComponent;
};