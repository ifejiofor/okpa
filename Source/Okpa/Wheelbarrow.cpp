// Wheelbarrow.cpp Wheelbarrow class implementation.

#include "Wheelbarrow.h"
#include "UObject/ConstructorHelpers.h"
#include "Engine/World.h"
#include "Kismet/GameplayStatics.h"
#include "OkpaGameMode.h"


AWheelbarrow::AWheelbarrow()
{
	static ConstructorHelpers::FObjectFinder<UStaticMesh> ObjectFinder(*UOkpaLibrary::GetReferencePathOfAsset(TEXT("WheelBarrowMesh")));
	RenderComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshOfWheelBarrow"));

	if (RenderComponent != nullptr) {
		RenderComponent->SetStaticMesh(ObjectFinder.Object);

		if (RootComponent != nullptr) {
			RootComponent->DestroyComponent();
		}

		RootComponent = RenderComponent;
	}
}


void AWheelbarrow::HoldOkpaPiece(AOkpaPiece* OkpaPiece)
{
	static const TArray<FVector> HoldLocations = {
		FVector(100, -20, 120), FVector(100, 20, 120), FVector(130, -20, 120), FVector(130, 20, 120), FVector(160, -20, 120), FVector(160, 20, 120),
		FVector(100, -20, 150), FVector(100, 20, 150), FVector(130, -20, 150), FVector(130, 20, 150) , FVector(160, -20, 150), FVector(160, 20, 150)
	};

	if (!ActorIsBeingHeld(OkpaPiece)) {
		int index = (GetNumberOfActorsThatAreBeingHeld() < sizeof(HoldLocations)) ? GetNumberOfActorsThatAreBeingHeld() : (sizeof(HoldLocations) - 1);
		HoldActor(OkpaPiece, HoldLocations[index]);
	}
}


AOkpaPiece* AWheelbarrow::RemoveOneOkpaPiece()
{
	return Cast<AOkpaPiece>(StopHoldingActor());
}


void AWheelbarrow::StopHoldingAllHeldOkpaPieces()
{
	while (!IsEmpty()) {
		RemoveOneOkpaPiece();
	}
}