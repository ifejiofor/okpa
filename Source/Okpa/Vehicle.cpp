// Vehicle.cpp: Vehicle class implementation.

#include "Vehicle.h"


AVehicle::AVehicle()
{
	SetCapacity(1);
}


void AVehicle::SetCapacity(int NewCapacity)
{
	if (NewCapacity < 0) {
		NewCapacity = 0;
	}

	if (VehicleSlots.Num() > NewCapacity) {
		for (int i = NewCapacity; i < VehicleSlots.Num(); ++i) {
			StopHoldingActor();
		}
	}

	VehicleSlots.Shrink();
	VehicleSlots.Reserve(NewCapacity);
}


int AVehicle::GetCapacity() const
{
	return VehicleSlots.Max();
}


int AVehicle::GetNumberOfActorsThatAreBeingHeld() const
{
	return VehicleSlots.Num();
}


bool AVehicle::HoldActor(AActor* NewActor, const FVector& RelativeLocationWhereNewActorShouldBeHeld)
{
	FDataAboutActorHeldByVehicle DataAboutNewActor;

	if (!IsFull() && NewActor != nullptr) {
		NewActor->SetActorLocation(this->GetActorLocation() + RelativeLocationWhereNewActorShouldBeHeld);

		DataAboutNewActor.Actor = NewActor;
		DataAboutNewActor.RelativeLocation = RelativeLocationWhereNewActorShouldBeHeld;

		VehicleSlots.Add(DataAboutNewActor);
		return true;
	}

	return false;
}


AActor* AVehicle::StopHoldingActor(AActor* Actor)
{
	if (Actor == nullptr && !IsEmpty()) {
		FDataAboutActorHeldByVehicle PoppedData = VehicleSlots.Pop(false);
		return PoppedData.Actor;
	}
	else if (ActorIsBeingHeld(Actor)) {
		VehicleSlots.RemoveAt(GetIndexOfVehicleSlotOccupiedByActor(Actor), 1, false);
		return Actor;
	}

	return nullptr;
}


bool AVehicle::ActorIsBeingHeld(AActor* Actor) const
{
	return GetIndexOfVehicleSlotOccupiedByActor(Actor) != INDEX_NONE;
}


int AVehicle::GetIndexOfVehicleSlotOccupiedByActor(const AActor* Actor) const
{
	for (int index = 0; index < VehicleSlots.Num(); ++index) {
		if (VehicleSlots[index].Actor == Actor) {
			return index;
		}
	}

	return INDEX_NONE;
}


bool AVehicle::IsFull() const
{
	return VehicleSlots.GetSlack() == 0;
}


bool AVehicle::IsEmpty() const
{
	return VehicleSlots.Num() == 0;
}


void AVehicle::Tick(float DeltaTime)
{
	bool IsMovingAtStartOfThisTick = IsMoving();

	Super::Tick(DeltaTime);

	if (IsMovingAtStartOfThisTick && !IsEmpty()) {
		SyncronizeLocationsOfHeldActors();
	}
}


void AVehicle::SyncronizeLocationsOfHeldActors()
{
	for (int i = 0; i < VehicleSlots.Num(); ++i) {
		VehicleSlots[i].Actor->SetActorLocation(this->GetActorLocation() + VehicleSlots[i].RelativeLocation);
	}
}


bool AVehicle::SetActorLocation(const FVector& NewLocation, bool bSweep, FHitResult* OutSweepHitResult, ETeleportType Teleport)
{
	bool Status = Super::SetActorLocation(NewLocation, bSweep, OutSweepHitResult, Teleport);
	SyncronizeLocationsOfHeldActors();
	return Status;
}