// OkpaGameMode.h: OkpaGameMode class definition.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Blueprint/UserWidget.h"
#include "Sound/AmbientSound.h"
#include "Wheelbarrow.h"
#include "OkpaPiece.h"
#include "Catapult.h"
#include "Bowl.h"
#include "TrackerOfOkpaPieces.h"
#include "ManagerOfSublevels.h"
#include "OkpaSaveGame.h"
#include "OkpaGameMode.generated.h"


/**
 * OkpaGameMode defines the gameplay aspects of the Okpa game.
 * @author Ejiofor, Ifechukwu.
 */
UCLASS()
class OKPA_API AOkpaGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	/** Sets default values for this game mode's properties. */
	AOkpaGameMode();

	/** Performs the processing that is to be performed after an actor, that was moving, arrives at its destination. */
	UFUNCTION()
	void ProcessActorDestinationArrival(AActor* ActorThatArrivedAtDestination, const FVector& Destination);

	/** Performs the processing that is to be performed after an actor hits another actor. */
	UFUNCTION()
	void ProcessActorHit(AActor* ActorThatHitAnotherActor, AActor* ActorThatWasHit, UPrimitiveComponent* ComponentThatHitAnotherComponent, UPrimitiveComponent* ComponentThatWasHit);
	
	/** Performs the processing that is to be performed after an okpa piece is launched by a catapult trigger. */
	UFUNCTION()
	void ProcessOkpaPieceLaunch(AOkpaPiece* OkpaPieceThatWasLaunched);

	/** Performs the processing that is to be performed after an okpa piece have been eaten. */
	UFUNCTION()
	void ProcessOkpaPieceEat(AOkpaPiece* OkpaPieceThatWasEaten);

	/** Performs the processing that is to be performed after a paper flipbook animation finishes. */
	UFUNCTION()
	void ProcessPaperFlipbookAnimationFinish();

	/** Displays the home screen of this game. */
	UFUNCTION(BlueprintCallable)
	void DisplayHomeScreen();

	/** Displays the menu screen of this game. */
	UFUNCTION(BlueprintCallable)
	void DisplayMenuScreen();

	/** Displays the screen that tells the story of this game. */
	UFUNCTION(BlueprintCallable)
	void DisplayStoryScreen();

	/** Displays the marketplace screen from where the game player can purchase in-game assets. */
	UFUNCTION(BlueprintCallable)
	void DisplayMarketplaceScreen();

	/** Purchases the specified booster.
		@return True if the booster was purchased successfully. False otherwise. */
	UFUNCTION(BlueprintCallable)
	bool PurchaseBooster(const FName& BoosterToPurchase);

	/** Removes the specified booster from the boosters owned by the game player. */
	UFUNCTION(BlueprintCallable)
	void RemoveFromBoostersOwnedByGamePlayer(const FName& BoosterToRemove);

	/** Deploys the specified booster so that the game player can use it. */
	UFUNCTION(BlueprintCallable)
	void DeployBooster(const FName& BoosterToDeploy);

	/** Restarts the current game session. */
	UFUNCTION(BlueprintCallable)
	void RestartGameSession();

	/** Pauses the current game session. */
	UFUNCTION(BlueprintCallable)
	void PauseGameSession();

	/** Resumes the current game session if it had been paused. */
	UFUNCTION(BlueprintCallable)
	void ResumeGameSession();

	/** Opens a sublevel. */
	UFUNCTION(BlueprintCallable)
	void OpenSublevel(int IdentifierOfSublevelThatShouldBeOpened);

	/** Returns the identifier of the current sublevel. */
	UFUNCTION(BlueprintCallable)
	int GetIdentifierOfCurrentSublevel() const;

	/** Returns the identifier of the highest sublevel that has ever been completed in this game. */
	UFUNCTION(BlueprintCallable)
	int GetIdentifierOfHighestSublevelThatHasEverBeenCompleted() const;

	/** Returns the highest count of okpa pieces that have ever been eaten in the specified sublevel. */
	UFUNCTION(BlueprintCallable)
	int GetHighestCountOfOkpaPiecesThatHaveEverBeenEatenInSublevel(int SublevelIdentifier) const;

	/** Returns the count of okpa pieces that have been eaten so far during the current game session.
	    @note Returns -1 if no game is currently in session. */
	UFUNCTION(BlueprintCallable)
	int GetCountOfOkpaPiecesEatenDuringCurrentGameSession() const;

	/** Returns the minimum count of eaten okpa pieces that must be achieved for the current game session to be won. */
	UFUNCTION(BlueprintCallable)
	int GetMinimumCountOfEatenOkpaPiecesRequiredToWinCurrentGameSession() const;

	/** Returns the amount of cowries that the game player should be rewarded for their achievement in the current game session. */
	UFUNCTION(BlueprintCallable)
	int GetCowrieRewardForAchievementInCurrentGameSession() const;

	/** Returns the amount of cowries that are currently owned by the game player. */
	UFUNCTION(BlueprintCallable)
	int GetAmountOfCowriesOwnedByGamePlayer() const;

	/** Returns data about all the boosters that are currently owned by the game player. */
	UFUNCTION(BlueprintCallable)
	TArray<FDataAboutOkpaGameBooster> GetDataAboutBoostersOwnedByGamePlayer() const;

	/** Returns the quantity that is currently owned, of the specified booster, by the game player. */
	UFUNCTION(BlueprintCallable)
	int GetQuantityOfBoosterOwnedByGamePlayer(const FName& NameOfBooster) const;

	/** Returns data about all the boosters that are sold in the marketplace of the Okpa game. */
	UFUNCTION(BlueprintCallable)
	TArray<FDataAboutOkpaGameBooster> GetDataAboutBoostersSoldInMarketplace() const;

	/** Returns data about the specified booster. */
	UFUNCTION(BlueprintCallable)
	FDataAboutOkpaGameBooster GetDataAboutBooster(const FName& NameOfBooster) const;

	/** Returns the capacity of the main wheelbarrow. */
	UFUNCTION(BlueprintCallable)
	int GetCapacityOfMainWheelbarrow() const;

	/** Returns the count of okpa pieces that are yet to be launched. */
	UFUNCTION(BlueprintCallable)
	int GetCountOfOkpaPiecesThatAreYetToBeLaunched() const;

	/** Returns the time elapsed (in seconds) since the current game session started.
		@note Returns -1 if no game is currently in session. */
	UFUNCTION(BlueprintCallable)
	float GetTimeElapsedSinceCurrentGameSessionStarted() const;

	/** Returns the base location of the trigger of the main catapult. */
	FVector GetBaseLocationOfMainCatapultTrigger() const;

	/** Returns true if the specified okpa piece has never been launched since it was spawned. Returns false otherwise. */
	bool OkpaPieceHasNeverBeenLaunched(AOkpaPiece* OkpaPiece) const;

	/** Returns true if the specified okpa piece is inside a bowl. Returns false otherwise. */
	bool OkpaPieceIsInsideBowl(AOkpaPiece* OkpaPiece) const;

	/** Returns true if the specified component is the enterance of the main bowl. Returns false otherwise. */
	bool ComponentIsEnteranceOfMainBowl(const UPrimitiveComponent* Component) const;

	/** Performs the processing that is to be performed periodically. */
	virtual void Tick(float DeltaTime) override;

protected:
	/** Performs the processing that is to be performed when this game mode spawned. */
	virtual void BeginPlay() override;

	/** Initializes this game.
	    @return	True, if successfully initialized this game. False, otherwise. */
	bool InitializeGame();

	/** Returns the initial achievement of the game player. */
	UOkpaSaveGame* GetGamePlayerInitialAchievement() const;

	/** Starts a session of this game. */
	void StartGameSession();

	/** Stops the current game session. */
	void StopGameSession();

	/** Closes the current sublevel if it is already opened. */
	void CloseCurrentSublevel();

	/** Saves, into slot, the achievement that the game player made in the current game session.
		@note This function should be called only when the game player successfully completes a game session. */
	void SaveGamePlayerAchievementOfCurrentSessionIntoSlot();

	/** Spawns some new okpa pieces and inserts the new okpa pieces into the main wheelbarrow. */
	void InsertNewOkpaPiecesIntoMainWheelbarrow(int CountOfNewOkpaPiecesToInsert);

	/** Transfers one okpa piece from the main wheelbarrow to the main catapult trigger.
		@return Pointer to the okpa piece that was transferred, if successful. Nullptr, otherwise. */
	AOkpaPiece* TransferOneOkpaPieceFromMainWheelbarrowToMainCatapultTrigger();

	/** Updates the information displayed in the GameSessionScreen. */
	void UpdateGameSessionScreen() const;

protected:
	/** Widget that displays when a game is in session. */
	UPROPERTY()
	UUserWidget* GameSessionScreen;

	/** Widget that displays when a game session is paused. */
	UPROPERTY()
	UUserWidget* PauseScreen;

	/** Widget that displays when the user successfully completes a sublevel. */
	UPROPERTY()
	UUserWidget* SuccessScreen;

	/** Widget that displays when the user fails to complete a sublevel. */
	UPROPERTY()
	UUserWidget* FailureScreen;

	/** Wheelbarrow that is actively used in this game. */
	UPROPERTY()
	AWheelbarrow* MainWheelbarrow;

	/** Catapult that is actively used in this game. */
	UPROPERTY()
	ACatapult* MainCatapult;

	/** Bowl that is actively used in this game. */
	UPROPERTY()
	ABowl* MainBowl;

	/** Ambient sound that is actively used in this game. */
	UPROPERTY()
	AAmbientSound* MainAmbientSound;

	/** Object that this game mode uses to manage the sublevels of this game. */
	UPROPERTY()
	UManagerOfSublevels* ManagerOfSublevels;

	/** Object that this game mode uses to keep track of every okpa piece in this game's world. */
	UPROPERTY()
	UTrackerOfOkpaPieces* TrackerOfOkpaPieces;

	/** Object that this game mode uses to save the achievement of the game player. */
	UPROPERTY()
	UOkpaSaveGame* GamePlayerAchievement;

	/** Flag that indicates whether or not a game session is currently in progress. */
	bool GameSessionIsCurrentlyInProgress;

	/** Number of seconds between when level began play till when the current game session started.
		@note The value of this property is meaningless whenever no game session is in progress. */
	float TimeWhenCurrentGameSessionStarted;

	/** Number of seconds since an okpa piece last landed in the current game session.
		@note An okpa piece has landed if it has either littered or entered inside bowl.
		@note The value of this property is -1 if no game session is in progress or if a game session is in progress and no okpa piece have been yet landed. */
	float TimeSinceOkpaPieceLastLandedInCurrentGameSession;

	/** Count of okpa pieces that have been eaten so far during the current game session. */
	int CountOfOkpaPiecesEatenDuringCurrentGameSession;

	/** Texts that that are displayed in a callout when target is hit to emphasize the deliciousness of okpa. */
	static const TArray<FString> TextsThatEmphasizeDeliciousnessOfOkpa;
};