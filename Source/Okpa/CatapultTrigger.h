// CatapultTrigger.h: CatapultTrigger class definition.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Classes/PaperSpriteComponent.h"
#include "Components/AudioComponent.h"
#include "Catapult.h"
#include "OkpaPiece.h"
#include "CatapultTrigger.generated.h"


/**
 * CatapultTrigger defines a trigger that can be owned by a catapult and used to launch okpa pieces.
 * @author Ejiofor,Ifechukwu.
 */
UCLASS()
class OKPA_API ACatapultTrigger : public APawn
{
	GENERATED_BODY()
	
public:
	/** Sets default values for this trigger's properties. */
	ACatapultTrigger();

	/** Reinitializes this trigger. */
	virtual void Reinitialize();

	/** Sets the catapult that owns this trigger. */
	void SetParentCatapult(ACatapult* NewParentCatapult);

	/** Causes this trigger to hold an okpa piece.
		@return True, if the okpa piece was successfully held. False, otherwise. */
	bool HoldOkpaPiece(AOkpaPiece* OkpaPiece);

	/** Causes this trigger to stop holding the okpa piece that it is currently holding. */
	void StopHoldingHeldOkpaPiece();

	/** Causes this trigger to be gripped (if this trigger is owned by a parent catapult and touch/mouse input occurred over this trigger). */
	void Grip();

	/** Causes this trigger to be ungripped. */
	void Ungrip();

	/** Returns true if this trigger is empty. Returns false otherwise.
		@note This trigger is empty whenever it is not holding any okpa piece. */
	bool IsEmpty() const;

	/** Returns true if this trigger is not empty. Returns false otherwise. */
	bool IsNotEmpty() const;

	/** Returns true if this trigger is holding the specified okpa piece. Returns false otherwise. */
	bool IsHoldingOkpaPiece(const AOkpaPiece* OkpaPiece) const;

	/** Deletes the projectile path of this trigger. */
	void DeleteProjectilePath();

	/** Sets the length of the projectile path of this trigger. */
	void SetLengthOfProjectilePath(float Length);

	/** Performs the processing that is to be performed periodically. */
	virtual void Tick(float DeltaSeconds) override;

protected:
	/** Sets up the player input component of this trigger. */
	virtual void SetupPlayerInputComponent(UInputComponent* PlayerInputComponent) override;

	/** Updates the transform of this trigger. */
	void UpdateTransform();

	/** Updates the transform of the specified elastic band.
		@note If the specified component is not an elastic band of this trigger, then this function does nothing. */
	void UpdateTransformOfElasticBand(UPaperSpriteComponent* ElasticBand);

	/** Updates the transform of the puller of this trigger. */
	void UpdateTransformOfPuller();

	/** Pulls this trigger to the touch finger position (if touch input is being used) or the mouse cursor position (if mouse input is being used).
		@param Value between -1 and +1 that represents the amount of displacement of the touch finger or the mouse cursor during the last frame. */
	void Pull(float AxisValue);

	/** Returns true if this trigger has been pulled significantly. Returns false otherwise. */
	bool HasBeenPulledSignificantly() const;

	/** Launches the held okpa piece so that it starts moving along a projectile path. */
	void LaunchHeldOkpaPiece();

	/** Moves this trigger, as well as its held okpa piece, to a new location.
		@note If the new location is beyond the pull limit of this trigger, then this trigger is only moved to the farthest location allowed by the pull limit. */
	void MoveTo(const FVector& NewLocation);

	/** Returns a location got by clamping the specified location to the pull limit of this trigger. */
	FVector ClampLocationToPullLimit(const FVector& Location) const;

	/** Calculates the velocity at which the held okpa piece will be launched if it is launched, now, by this trigger. */
	void CalculateLaunchVelocity();

	/** Creates a visible representation of the projectile path through which the held okpa piece will move along if it is launched now by this trigger. */
	void ShowProjectilePath();

	/** Returns true if this trigger is touched from a touch screen or hovered with a mouse. Returns false otherwise. */
	bool TouchInputOrMouseInputOccurredOverThisTrigger() const;

protected:
	/** Component that is used to render one of the elastic bands of this trigger. */
	UPROPERTY(VisibleAnywhere)
	UPaperSpriteComponent* FirstElasticBand;

	/** Component that is used to render the other elastic band of this trigger. */
	UPROPERTY(VisibleAnywhere)
	UPaperSpriteComponent* SecondElasticBand;

	/** Component that is used to render the place that the held okpa piece of this trigger rests in. */
	UPROPERTY(VisibleAnywhere)
	UPaperSpriteComponent* Puller;

	/** Catapult that owns this trigger. */
	UPROPERTY()
	ACatapult* ParentCatapult;

	/** Okpa piece that is being held in this trigger. */
	UPROPERTY()
	AOkpaPiece* HeldOkpaPiece;

	/** Sound that plays when this trigger begins to be pulled. */
	UPROPERTY()
	USoundBase* PullSound;

	/** Sound that plays when this trigger is ungripped after it had been pulled significantly. */
	UPROPERTY()
	USoundBase* UngripSound;

	/** Audio component that is used to manipulate the pull sound that is currently playing. */
	UPROPERTY()
	UAudioComponent* PullSoundThatIsCurrentlyPlaying;

	/** Length (along the X-axis) of the elastic band of this trigger when the scale of the elastic band is set to 1.0. */
	float UnscaledLengthOfElasticBand;

	/** Length (along the X-axis) of the puller of this trigger when the scale of the puller is set to 1.0. */
	float UnscaledLengthOfPuller;

	/** Size of the bounding box of a typical okpa piece mesh. */
	FVector SizeOfTypicalOkpaPiece;

	/** Length of the projectile path of this trigger. */
	float LengthOfProjectilePath;

	/** Flag that indicates whether or not this trigger is gripped. */
	bool IsGripped;

	/** Launch velocity of this trigger.
		@see CalculateLaunchVelocity. */
	FVector LaunchVelocity;
};