// CatapultTrigger.cpp: CatapultTrigger class implementation.

#include "CatapultTrigger.h"
#include "UObject/ConstructorHelpers.h"
#include "Classes/PaperSprite.h"
#include "Engine/StaticMesh.h"
#include "Components/InputComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Math/UnrealMathUtility.h"
#include "OkpaLibrary.h"
#include "OkpaGameMode.h"
#include "OkpaHUD.h"

#define NAME_OF_FIRST_SOCKET_OF_CATAPULT							TEXT("FirstBranch")
#define NAME_OF_SECOND_SOCKET_OF_CATAPULT							TEXT("SecondBranch")
#define MAXIMUM_ALLOWABLE_LENGTH_OF_CATAPULT_TRIGGER				200.0f
#define MAXIMUM_ALLOWABLE_THICKNESS_OF_CATAPULT_TRIGGER				1.7f
#define UNIT_LAUNCH_VELOCITY_OF_CATAPULT_TRIGGER					6
#define AREA_OF_CATAPULT_TRIGGER									100


ACatapultTrigger::ACatapultTrigger()
	: ParentCatapult(nullptr), HeldOkpaPiece(nullptr), PullSoundThatIsCurrentlyPlaying(nullptr), IsGripped(false)
{
	static ConstructorHelpers::FObjectFinder<UPaperSprite> ElasicBandObjectFinder(*UOkpaLibrary::GetReferencePathOfAsset(TEXT("CatapultElasticBandSprite")));
	static ConstructorHelpers::FObjectFinder<UPaperSprite> PullerObjectFinder(*UOkpaLibrary::GetReferencePathOfAsset(TEXT("CatapultPullerSprite")));

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	FirstElasticBand = CreateDefaultSubobject<UPaperSpriteComponent>(TEXT("FirstElasticBand"));
	SecondElasticBand = CreateDefaultSubobject<UPaperSpriteComponent>(TEXT("SecondElasticBand"));
	Puller = CreateDefaultSubobject<UPaperSpriteComponent>(TEXT("Puller"));
	
	if (FirstElasticBand != nullptr && SecondElasticBand != nullptr && Puller != nullptr) {
		FirstElasticBand->SetSprite(ElasicBandObjectFinder.Object);
		SecondElasticBand->SetSprite(ElasicBandObjectFinder.Object);
		Puller->SetSprite(PullerObjectFinder.Object);
	}

	if (FirstElasticBand != nullptr && FirstElasticBand->GetSprite() != nullptr) {
		UnscaledLengthOfElasticBand = FirstElasticBand->GetSprite()->GetRenderBounds().GetBox().GetSize().X;
	}

	if (Puller != nullptr && Puller->GetSprite() != nullptr) {
		Puller->SetupAttachment(RootComponent);
		UnscaledLengthOfPuller = Puller->GetSprite()->GetRenderBounds().GetBox().GetSize().X;
	}

	SizeOfTypicalOkpaPiece = UOkpaLibrary::GetSizeOfTypicalOkpaPiece();
	PullSound = LoadObject<USoundBase>(nullptr, *UOkpaLibrary::GetReferencePathOfAsset(TEXT("CatapultTriggerPullSound")));
	UngripSound = LoadObject<USoundBase>(nullptr, *UOkpaLibrary::GetReferencePathOfAsset(TEXT("CatapultTriggerUngripSound")));
	SetLengthOfProjectilePath(LONGEST_ATTAINABLE_LENGTH_OF_PROJECTILE_PATH);
	SetActorHiddenInGame(true);
}


void ACatapultTrigger::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	PlayerInputComponent->BindAction(TEXT("GripOrUngripCatapultTrigger"), IE_Pressed, this, &ACatapultTrigger::Grip);
	PlayerInputComponent->BindAction(TEXT("GripOrUngripCatapultTrigger"), IE_Released, this, &ACatapultTrigger::Ungrip);
	PlayerInputComponent->BindAxis(TEXT("PullCatapultTrigger"), this, &ACatapultTrigger::Pull);
}


void ACatapultTrigger::UpdateTransform()
{
	SetActorRotation(FRotator::ZeroRotator);
	UpdateTransformOfElasticBand(FirstElasticBand);
	UpdateTransformOfElasticBand(SecondElasticBand);
	UpdateTransformOfPuller();
}


void ACatapultTrigger::UpdateTransformOfElasticBand(UPaperSpriteComponent* ElasticBand)
{
	if (ElasticBand == nullptr) {
		return;
	}

	if (ElasticBand != FirstElasticBand && ElasticBand != SecondElasticBand) {
		return;
	}

	if (ParentCatapult == nullptr) {
		ElasticBand->SetWorldScale3D(FVector::ZeroVector);
		return;
	}

	FVector LocationOfRoot = GetActorLocation();
	FVector LocationOfElasticBand = ElasticBand->GetComponentLocation();
	FVector ScaleOfElasticBand(1);
	FRotator RotationOfElasticBand(0);

	if (HasBeenPulledSignificantly()) {
		LocationOfRoot.Y += GetSign(LocationOfElasticBand.Y - LocationOfRoot.Y) * SizeOfTypicalOkpaPiece.Y / 2;
	}

	ScaleOfElasticBand.X = FVector::Distance(LocationOfRoot, LocationOfElasticBand) / UnscaledLengthOfElasticBand;
	ScaleOfElasticBand.Z = FMath::Min(1 / ScaleOfElasticBand.X, MAXIMUM_ALLOWABLE_THICKNESS_OF_CATAPULT_TRIGGER);

	if (FMath::IsNearlyEqual(LocationOfRoot.X, LocationOfElasticBand.X)) {
		RotationOfElasticBand.Pitch = (RADIANS_TO_DEGREES_CONVERSION_FACTOR * FMath::Atan((LocationOfRoot.Z - LocationOfElasticBand.Z) / (LocationOfRoot.Y - LocationOfElasticBand.Y))) + (LocationOfRoot.Y < LocationOfElasticBand.Y ? 180 : 0);
		RotationOfElasticBand.Yaw = RotationOfElasticBand.Roll = 90;
	}
	else {
		RotationOfElasticBand.Pitch = (RADIANS_TO_DEGREES_CONVERSION_FACTOR * FMath::Atan((LocationOfRoot.Z - LocationOfElasticBand.Z) / (LocationOfRoot.X - LocationOfElasticBand.X))) + (LocationOfRoot.X < LocationOfElasticBand.X ? 180 : 0);
		RotationOfElasticBand.Yaw = RADIANS_TO_DEGREES_CONVERSION_FACTOR * FMath::Atan((LocationOfRoot.Y - LocationOfElasticBand.Y) / (LocationOfRoot.X - LocationOfElasticBand.X));
	}

	ElasticBand->SetWorldScale3D(ScaleOfElasticBand);
	ElasticBand->SetRelativeRotation(RotationOfElasticBand);
}


void ACatapultTrigger::UpdateTransformOfPuller()
{
	if (Puller == nullptr) {
		return;
	}

	if (ParentCatapult == nullptr || !HasBeenPulledSignificantly()) {
		Puller->SetWorldScale3D(FVector::ZeroVector);
		return;
	}

	FVector LocationOfPuller = GetActorLocation();
	FVector ScaleOfPuller(1);
	FRotator RotationOfPuller(0);

	LocationOfPuller.Y += SizeOfTypicalOkpaPiece.Y / 2;

	ScaleOfPuller.Y = ScaleOfPuller.X = SizeOfTypicalOkpaPiece.X / UnscaledLengthOfPuller;

	if (FMath::IsNearlyZero(LaunchVelocity.X)) {
		RotationOfPuller.Pitch = 90;
	}
	else {
		RotationOfPuller.Pitch = (RADIANS_TO_DEGREES_CONVERSION_FACTOR * FMath::Atan(LaunchVelocity.Z / LaunchVelocity.X)) + (LaunchVelocity.X < 0 ? 180 : 0);
	}

	Puller->SetWorldScale3D(ScaleOfPuller);
	Puller->SetWorldLocationAndRotation(LocationOfPuller, RotationOfPuller);
}


void ACatapultTrigger::SetParentCatapult(ACatapult* NewParentCatapult)
{
	ParentCatapult = NewParentCatapult;

	if (ParentCatapult == nullptr) {
		IsGripped = false;
		return;
	}

	if (FirstElasticBand == nullptr || SecondElasticBand == nullptr) {
		return;
	}

	FirstElasticBand->AttachToComponent(ParentCatapult->GetRenderComponent(), FAttachmentTransformRules::SnapToTargetNotIncludingScale, NAME_OF_FIRST_SOCKET_OF_CATAPULT);
	SecondElasticBand->AttachToComponent(ParentCatapult->GetRenderComponent(), FAttachmentTransformRules::SnapToTargetNotIncludingScale, NAME_OF_SECOND_SOCKET_OF_CATAPULT);
	UpdateTransform();
}


void ACatapultTrigger::Reinitialize()
{
	Ungrip();
	DeleteProjectilePath();
	SetParentCatapult(nullptr);
	SetLengthOfProjectilePath(LONGEST_ATTAINABLE_LENGTH_OF_PROJECTILE_PATH);
	SetActorHiddenInGame(true);
}


void ACatapultTrigger::Ungrip()
{
	if (!IsGripped) {
		return;
	}

	IsGripped = false;

	if (HasBeenPulledSignificantly()) {
		if (PullSoundThatIsCurrentlyPlaying != nullptr) {
			PullSoundThatIsCurrentlyPlaying->DestroyComponent();
			PullSoundThatIsCurrentlyPlaying = nullptr;
		}

		UGameplayStatics::PlaySoundAtLocation(this, UngripSound, GetActorLocation());
		MoveTo(ParentCatapult->GetBaseLocationOfTrigger());

		if (IsNotEmpty()) {
			LaunchHeldOkpaPiece();
		}
	}
	else {
		MoveTo(ParentCatapult->GetBaseLocationOfTrigger());
	}

	CalculateLaunchVelocity();
	UpdateTransform();
}


bool ACatapultTrigger::HasBeenPulledSignificantly() const
{
	return !(GetActorLocation().Equals(ParentCatapult->GetBaseLocationOfTrigger(), 15));
}


void ACatapultTrigger::MoveTo(const FVector& NewLocation)
{
	FVector ClampedLocation = ClampLocationToPullLimit(NewLocation);
	this->SetActorLocation(ClampedLocation);

	if (IsNotEmpty()) {
		HeldOkpaPiece->SetActorLocation(ClampedLocation);
	}
}


FVector ACatapultTrigger::ClampLocationToPullLimit(const FVector& SpecifiedLocation) const
{
	float PullLength;
	FVector ResultingLocation;
	FVector PullComponents = SpecifiedLocation - ParentCatapult->GetBaseLocationOfTrigger();

	PullLength = FMath::Sqrt((PullComponents.Z * PullComponents.Z) + (PullComponents.X * PullComponents.X));
	PullLength = FMath::Clamp(PullLength, 0.0f, MAXIMUM_ALLOWABLE_LENGTH_OF_CATAPULT_TRIGGER);

	if (FMath::IsNearlyZero(PullComponents.X)) {
		ResultingLocation.X = 0;
		ResultingLocation.Y = 0;
		ResultingLocation.Z = GetSign(PullComponents.Z) * PullLength;
	}
	else {
		float Slope = PullComponents.Z / PullComponents.X;
		ResultingLocation.X = GetSign(PullComponents.X) * FMath::Sqrt((PullLength * PullLength) / ((Slope * Slope) + 1));
		ResultingLocation.Y = 0;
		ResultingLocation.Z = Slope * ResultingLocation.X;
	}

	return ParentCatapult->GetBaseLocationOfTrigger() + ResultingLocation;
}


void ACatapultTrigger::LaunchHeldOkpaPiece()
{
	if (IsNotEmpty()) {
		AOkpaPiece* OkpaPieceThatIsBeingLaunched = HeldOkpaPiece;
		StopHoldingHeldOkpaPiece();
		OkpaPieceThatIsBeingLaunched->EnableSweepMovement();
		OkpaPieceThatIsBeingLaunched->StartMovingWithNewVelocity(LaunchVelocity);
		UOkpaLibrary::GetOkpaGameMode(this)->ProcessOkpaPieceLaunch(OkpaPieceThatIsBeingLaunched);
	}
}


void ACatapultTrigger::StopHoldingHeldOkpaPiece()
{
	HeldOkpaPiece = nullptr;
}


void ACatapultTrigger::Grip()
{
	if (ParentCatapult != nullptr && TouchInputOrMouseInputOccurredOverThisTrigger()) {
		IsGripped = true;
	}
}


bool ACatapultTrigger::TouchInputOrMouseInputOccurredOverThisTrigger() const
{
	return UOkpaLibrary::GetWorldLocationWhereTouchInputOrMouseInputOccurred(this).Equals(ParentCatapult->GetBaseLocationOfTrigger(), AREA_OF_CATAPULT_TRIGGER);
}


void ACatapultTrigger::Pull(float AxisValue)
{
	if (!IsGripped || FMath::IsNearlyEqual(AxisValue, 0.0f)) {
		return;
	}

	if (UOkpaLibrary::GetOkpaHUD(this)->TutorialIsBeingDisplayed()) {
		UOkpaLibrary::GetOkpaHUD(this)->StopDisplayingTutorial();
	}

	if (PullSoundThatIsCurrentlyPlaying == nullptr) {
		PullSoundThatIsCurrentlyPlaying = UGameplayStatics::SpawnSoundAtLocation(this, PullSound, GetActorLocation());
	}

	MoveTo(UOkpaLibrary::GetWorldLocationWhereTouchInputOrMouseInputOccurred(this));
	UpdateTransform();
	CalculateLaunchVelocity();
}


void ACatapultTrigger::CalculateLaunchVelocity()
{
	LaunchVelocity = (ParentCatapult->GetBaseLocationOfTrigger() - GetActorLocation()) * UNIT_LAUNCH_VELOCITY_OF_CATAPULT_TRIGGER;
}


void ACatapultTrigger::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	if (IsGripped) {
		if (HasBeenPulledSignificantly()) {
			ShowProjectilePath();
		}
		else {
			DeleteProjectilePath();
		}
	}
}


void ACatapultTrigger::ShowProjectilePath()
{
	FPredictProjectilePathParams ProjectilePathParameters(0, ParentCatapult->GetBaseLocationOfTrigger(), LaunchVelocity, LengthOfProjectilePath, ECC_WorldStatic);
	FPredictProjectilePathResult ProjectilePathResult;
	TArray<FVector> LocationsInProjectilePath;
	int CountOfLocationsInProjectilePath;

	DeleteProjectilePath();
	UGameplayStatics::PredictProjectilePath(this, ProjectilePathParameters, ProjectilePathResult);
	CountOfLocationsInProjectilePath = ProjectilePathResult.PathData.Num();

	for (int i = 0; i < CountOfLocationsInProjectilePath; ++i) {
		if (ProjectilePathResult.PathData[i].Location.Z >= Z_LOCATION_OF_RIVER_SURFACE) {
			LocationsInProjectilePath.Add(ProjectilePathResult.PathData[i].Location);
		}
	}

	UOkpaLibrary::GetOkpaHUD(this)->DisplayProjectilePath(LocationsInProjectilePath);
}


bool ACatapultTrigger::HoldOkpaPiece(AOkpaPiece* OkpaPiece)
{
	if (OkpaPiece != nullptr) {
		HeldOkpaPiece = OkpaPiece;
		return true;
	}
	else {
		return false;
	}
}


bool ACatapultTrigger::IsNotEmpty() const
{
	return !IsEmpty();
}


bool ACatapultTrigger::IsEmpty() const
{
	return HeldOkpaPiece == nullptr;
}


bool ACatapultTrigger::IsHoldingOkpaPiece(const AOkpaPiece* SpecifiedOkpaPiece) const
{
	return HeldOkpaPiece == SpecifiedOkpaPiece;
}


void ACatapultTrigger::DeleteProjectilePath()
{
	UOkpaLibrary::GetOkpaHUD(this)->StopDisplayingCurrentProjectilePath();
}


void ACatapultTrigger::SetLengthOfProjectilePath(float Length)
{
	if (Length == DEFAULT_LENGTH_OF_PROJECTILE_PATH) {
		switch (UOkpaLibrary::GetOkpaGameMode(this)->GetIdentifierOfCurrentSublevel()) {
		case FIRST_SUBLEVEL:
			Length = 0.85;
			break;
		case SECOND_SUBLEVEL:
		case THIRD_SUBLEVEL:
		case FOURTH_SUBLEVEL:
			Length = 0.5;
			break;
		case FIFTH_SUBLEVEL:
		case SIXTH_SUBLEVEL:
		case SEVENTH_SUBLEVEL:
			Length = 0.2;
			break;
		default:
			Length = 0.1;
			break;
		}
	}

	LengthOfProjectilePath = FMath::Clamp(Length, 0.0f, LONGEST_ATTAINABLE_LENGTH_OF_PROJECTILE_PATH);
}