// Wheelbarrow.h: Wheelbarrow class definition.

#pragma once

#include "CoreMinimal.h"
#include "Vehicle.h"
#include "OkpaPiece.h"
#include "Wheelbarrow.generated.h"

/**
 * Wheelbarrow defines a vehicle that is used in this game to hold okpa pieces.
 * @author EjioforIfechukwu.
 */
UCLASS()
class OKPA_API AWheelbarrow : public AVehicle
{
	GENERATED_BODY()

public:
	/** Sets default values for this wheelbarrow's properties. */
	AWheelbarrow();

	/** Causes this wheelbarrow to hold the specified okpa piece. */
	void HoldOkpaPiece(AOkpaPiece* OkpaPiece);

	/** Removes one okpa piece from this wheelbarrow.
		@return If successful, returns a pointer to the okpa piece that was removed. Otherwise, returns nullptr. */
	AOkpaPiece* RemoveOneOkpaPiece();

	/** Causes this wheelbarrow to stop holding all the okpa pieces that it is holding. */
	void StopHoldingAllHeldOkpaPieces();

protected:
	/** Component that is used to render this wheelbarrow. */
	UPROPERTY(VisibleAnywhere)
	UStaticMeshComponent* RenderComponent;
};