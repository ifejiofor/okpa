// Magnet.cpp: Magnet class implementation.

#include "Magnet.h"
#include "UObject/ConstructorHelpers.h"
#include "Classes/PaperSprite.h"
#include "Kismet/GameplayStatics.h"
#include "OkpaPiece.h"
#include "OkpaHUD.h"
#include "OkpaGameMode.h"


#define OkpaPieceIsWithinMagneticField(Actor)	(FVector::Distance(this->GetActorLocation(), (Actor)->GetActorLocation()) > 1 && FVector::Distance(this->GetActorLocation(), (Actor)->GetActorLocation()) <= RadiusOfField)


AMagnet::AMagnet()
	: RadiusOfField(DEFAULT_RADIUS_OF_MAGNETIC_FIELD), StrengthOfField(DEFAULT_STRENGTH_OF_MAGNETIC_FIELD)
{
	static ConstructorHelpers::FObjectFinder<UPaperSprite> ObjectFinder(*UOkpaLibrary::GetReferencePathOfAsset(TEXT("StrongMagnetSprite")));
	RenderComponent = CreateDefaultSubobject<UPaperSpriteComponent>(TEXT("RenderComponent"));

	if (RenderComponent != nullptr) {
		RenderComponent->SetSprite(ObjectFinder.Object);

		if (RootComponent != nullptr) {
			RootComponent->DestroyComponent();
		}

		RootComponent = RenderComponent;
	}

	PrimaryActorTick.bCanEverTick = true;
}


void AMagnet::BeginPlay()
{
	Super::BeginPlay();
	UOkpaLibrary::GetOkpaHUD(this)->DisplayMagneticField(this);
}


void AMagnet::SetRenderSprite(UPaperSprite* Sprite)
{
	RenderComponent->SetSprite(Sprite);
}


void AMagnet::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	TArray<AActor*> ExistingOkpaPieces;
	int CountOfExistingOkpaPieces;
	AOkpaPiece* OkpaPiece;
	FVector AttractionVelocity;
	AOkpaGameMode* GameMode = UOkpaLibrary::GetOkpaGameMode(this);

	UGameplayStatics::GetAllActorsOfClass(this, AOkpaPiece::StaticClass(), ExistingOkpaPieces);
	CountOfExistingOkpaPieces = ExistingOkpaPieces.Num();
	
	for (int i = 0; i < CountOfExistingOkpaPieces; ++i) {
		OkpaPiece = Cast<AOkpaPiece>(ExistingOkpaPieces[i]);

		if (OkpaPieceIsWithinMagneticField(OkpaPiece) && !GameMode->OkpaPieceIsInsideBowl(OkpaPiece)) {
			AttractionVelocity = StrengthOfField * (this->GetActorLocation() - ExistingOkpaPieces[i]->GetActorLocation()).GetSafeNormal();
			OkpaPiece->AddVelocity(AttractionVelocity);
		}
	}
}


void AMagnet::SetRadiusOfField(float NewRadiusOfField)
{
	RadiusOfField = NewRadiusOfField > 0 ? NewRadiusOfField : 1;
}


void AMagnet::SetStrengthOfField(float NewStrengthOfField)
{
	StrengthOfField = NewStrengthOfField > 0 ? NewStrengthOfField : 1;
}


float AMagnet::GetRadiusOfField() const
{
	return RadiusOfField;
}


float AMagnet::GetStrengthOfField() const
{
	return StrengthOfField;
}