// OkpaSaveGame.cpp: OkpaSaveGame class implementation.

#include "OkpaSaveGame.h"
#include "ManagerOfSublevels.h"

#define GetSublevelIndex(SublevelIdentifier)	((SublevelIdentifier) - (FIRST_SUBLEVEL))


bool UOkpaSaveGame::SetCountOfOkpaPiecesThatWereEatenInSublevel(int Sublevel, int NewCountOfOkpaPieces)
{
	if (Sublevel < FIRST_SUBLEVEL || Sublevel > GetIdentifierOfLastUnlockedSublevel() || NewCountOfOkpaPieces <= 0) {
		return false;
	}

	if (Sublevel == GetIdentifierOfLastUnlockedSublevel()) {
		HighestCountsOfOkpaPiecesThatEverHaveBeenEaten.Add(NewCountOfOkpaPieces);
	}
	else if (NewCountOfOkpaPieces > GetHighestCountOfOkpaPiecesThatHaveEverBeenEatenInSublevel(Sublevel)) {
		HighestCountsOfOkpaPiecesThatEverHaveBeenEaten[GetSublevelIndex(Sublevel)] = NewCountOfOkpaPieces;
	}

	return true;
}


int UOkpaSaveGame::GetIdentifierOfLastUnlockedSublevel() const
{
	return 1 + GetIdentifierOfHighestSublevelThatHasEverBeenCompleted();
}


int UOkpaSaveGame::GetIdentifierOfHighestSublevelThatHasEverBeenCompleted() const
{
	return HighestCountsOfOkpaPiecesThatEverHaveBeenEaten.Num();
}


int UOkpaSaveGame::GetHighestCountOfOkpaPiecesThatHaveEverBeenEatenInSublevel(int Sublevel) const
{
	if (Sublevel < FIRST_SUBLEVEL || Sublevel > GetIdentifierOfHighestSublevelThatHasEverBeenCompleted()) {
		return 0;
	}
	else {
		return HighestCountsOfOkpaPiecesThatEverHaveBeenEaten[GetSublevelIndex(Sublevel)];
	}
}


bool UOkpaSaveGame::AddToCowriesOwned(int AmountOfCowriesToAdd)
{
	if (AmountOfCowriesToAdd < 0) {
		return false;
	}

	AmountOfCowriesOwned += AmountOfCowriesToAdd;
	return true;
}


bool UOkpaSaveGame::RemoveFromCowriesOwned(int AmountOfCowriesToSubtract)
{
	if (AmountOfCowriesToSubtract < 0) {
		return false;
	}

	AmountOfCowriesOwned -= AmountOfCowriesToSubtract;
	return true;
}


int UOkpaSaveGame::GetAmountOfCowriesOwned() const
{
	return AmountOfCowriesOwned;
}


bool UOkpaSaveGame::AddToBoostersOwned(const FName& BoosterToAdd)
{
	if (!UOkpaLibrary::IsValidNameOfBooster(BoosterToAdd)) {
		return false;
	}

	int IndexOfBooster = NamesOfBoostersOwned.Find(BoosterToAdd);

	if (IndexOfBooster == INDEX_NONE) {
		NamesOfBoostersOwned.Add(BoosterToAdd);
		QuantitiesOfBoostersOwned.Add(1);
	}
	else {
		++QuantitiesOfBoostersOwned[IndexOfBooster];
	}

	return true;
}


bool UOkpaSaveGame::RemoveFromBoostersOwned(const FName& BoosterToRemove)
{
	if (!UOkpaLibrary::IsValidNameOfBooster(BoosterToRemove)) {
		return false;
	}

	int IndexOfBooster = NamesOfBoostersOwned.Find(BoosterToRemove);

	if (IndexOfBooster == INDEX_NONE) {
		return false;
	}

	if (QuantitiesOfBoostersOwned[IndexOfBooster] == 1) {
		NamesOfBoostersOwned.RemoveAt(IndexOfBooster);
		QuantitiesOfBoostersOwned.RemoveAt(IndexOfBooster);
	}
	else {
		--QuantitiesOfBoostersOwned[IndexOfBooster];
	}

	return true;
}


const TArray<FName>& UOkpaSaveGame::GetNamesOfBoostersOwned() const
{
	return NamesOfBoostersOwned;
}


int UOkpaSaveGame::GetQuantityOwnedOfBooster(const FName& NameOfBooster) const
{
	int IndexOfBooster = NamesOfBoostersOwned.Find(NameOfBooster);

	if (IndexOfBooster == INDEX_NONE) {
		return 0;
	}
	else {
		return QuantitiesOfBoostersOwned[IndexOfBooster];
	}
}