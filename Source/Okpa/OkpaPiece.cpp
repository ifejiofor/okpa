// OkpaPiece.h: OkpaPiece class implementation.

#include "OkpaPiece.h"
#include "UObject/ConstructorHelpers.h"
#include "Kismet/GameplayStatics.h"
#include "Classes/PaperFlipbook.h"
#include "Engine/StaticMesh.h"
#include "OkpaLibrary.h"
#include "OkpaGameMode.h"


#define IsRiver(x)	(Cast<AActor>(x) != nullptr && (x)->GetName() == NAME_OF_RIVER)


AOkpaPiece::AOkpaPiece()
{
	static ConstructorHelpers::FObjectFinder<UStaticMesh> ObjectFinder(*UOkpaLibrary::GetReferencePathOfAsset(TEXT("OkpaPieceMesh")));
	RenderComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("RenderComponent"));

	if (RenderComponent != nullptr) {
		RenderComponent->SetStaticMesh(ObjectFinder.Object);
		RenderComponent->SetNotifyRigidBodyCollision(true);
		RenderComponent->SetGenerateOverlapEvents(true);

		if (RootComponent != nullptr) {
			RootComponent->DestroyComponent();
		}

		RootComponent = RenderComponent;
	}
}


FVector AOkpaPiece::GetVelocity() const
{	
	if (RenderComponent != nullptr && RenderComponent->IsSimulatingPhysics()) {
		return RenderComponent->GetPhysicsLinearVelocity();
	}
	else {
		return AActorThatCanMove::GetVelocity();
	}
}


void AOkpaPiece::NotifyActorBeginOverlap(AActor* OtherActor)
{
	if (IsRiver(OtherActor) && !(this->GetVelocity().IsNearlyZero(MINIMUM_OKPA_SPEED_THAT_CAN_CAUSE_RIVER_TO_SPLASH))) {
		UGameplayStatics::PlaySoundAtLocation(this, LoadObject<USoundBase>(nullptr, *UOkpaLibrary::GetReferencePathOfAsset(TEXT("RiverSplashSound"))), this->GetActorLocation());
		UOkpaLibrary::PlayAnimationAtLocation(this, LoadObject<UPaperFlipbook>(nullptr, *UOkpaLibrary::GetReferencePathOfAsset(TEXT("RiverSplashAnimation"))), this->GetActorLocation());
	}
}


void AOkpaPiece::NotifyHit(UPrimitiveComponent* MyComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, bool bSelfMoved, FVector HitLocation, FVector HitNormal, FVector NormalImpulse, const FHitResult& Hit)
{
	AOkpaGameMode* GameMode = UOkpaLibrary::GetOkpaGameMode(this);

	if (!bSelfMoved || GameMode->OkpaPieceHasNeverBeenLaunched(this) || RenderComponent == nullptr) {
		return;
	}

	if (GameMode->ComponentIsEnteranceOfMainBowl(OtherComponent)) {
		StopMoving();
		RenderComponent->SetSimulatePhysics(false);
	}
	else if (IsMoving() && RenderComponent->IsSimulatingPhysics()) {
		RenderComponent->SetPhysicsLinearVelocity(FVector::ZeroVector);
		StopMoving();
	}
	else if (IsMoving() && !RenderComponent->IsSimulatingPhysics()) {
		RenderComponent->SetSimulatePhysics(true);
		RenderComponent->SetPhysicsLinearVelocity(0.02 * GetVelocity());
		StopMoving();
	}

	GameMode->ProcessActorHit(this, OtherActor, MyComponent, OtherComponent);
}