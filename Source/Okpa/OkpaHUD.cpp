// OkpaHUD.cpp: OkpaHUD class implementation.

#include "OkpaHUD.h"
#include "Engine/Engine.h"
#include "Engine/Canvas.h"
#include "Components/TextRenderComponent.h"
#include "FadeoutText.h"
#include "OkpaLibrary.h"
#include "OkpaGameMode.h"
#include "Magnet.h"


#define TIME_TO_SPEND_IN_INITIAL_STATE_OF_TUTORIAL			0.2
#define TIME_TO_SPEND_IN_GRIPPED_STATE_OF_TUTORIAL			1.0
#define TIME_TO_SPEND_IN_UNGRIPPED_STATE_OF_TUTORIAL		0.8
#define RESULTANT_VELOCITY_OF_TUTORIAL_FINGER				40.0
#define MAXIMUM_AGE_OF_CALLOUT								3.0
#define AGE_AT_WHICH_CALLOUT_ACHIEVES_MAXIMUM_SCALE			0.10
#define AGE_AT_WHICH_CALLOUT_ACHIEVES_STABLE_SCALE			0.12
#define MAXIMUM_SCALE_OF_CALLOUT							1.2
#define STABLE_SCALE_OF_CALLOUT								1.0
#define LEFT_PADDING_OF_CALLOUT								37.0
#define TOP_PADDING_OF_CALLOUT								24.0
#define SPEED_OF_ROTATION_OF_MAGNETIC_FIELD_RENDER				(PI / 15)
#define CurrentTutorialFingerHasArrivedAtItsFinalPosition	(GetSign(CurrentTutorial.FinalPositionOfFinger.X - CurrentTutorial.CurrentPositionOfFinger.X) != GetSign(CurrentTutorial.FinalPositionOfFinger.X - CurrentTutorial.InitialPositionOfFinger.X))


void FDataAboutOkpaTutorial::TransitionToState(EOkpaTutorialState NewState)
{
	CurrentState = NewState;
	TimeSpentInCurrentState = 0.0;

	if (CurrentState == INITIAL_STATE) {
		CurrentPositionOfFinger = InitialPositionOfFinger;
	}

	switch (CurrentState) {
	case INITIAL_STATE:
		CurrentText = TEXT("");
		break;

	case GRIP_STATE:
		CurrentText = TEXT("Press on the catapult trigger.");
		break;

	case PULL_STATE:
		CurrentText = TEXT("Pull it.");
		break;

	case UNGRIP_STATE:
		CurrentText = TEXT("Release it.");
		break;
	}
}


AOkpaHUD::AOkpaHUD()
	: CurrentRotationOfMagneticFieldRender(0)
{
	PrimaryActorTick.bCanEverTick = true;
}


void AOkpaHUD::BeginPlay()
{
	Super::BeginPlay();
	CalloutBackgroundTexture = LoadObject<UTexture2D>(nullptr, *UOkpaLibrary::GetReferencePathOfAsset(TEXT("CalloutSmallBackgroundImage")));
	ProjectilePointTexture = LoadObject<UTexture2D>(nullptr, *UOkpaLibrary::GetReferencePathOfAsset(TEXT("ProjectilePointImage")));
	FingerTexture = FingerPressTexture = MagneticFieldEdgePointTexture = nullptr;
}


void AOkpaHUD::DisplayTutorial()
{
	int WidthOfViewport, HeightOfViewport;

	GetWorld()->GetFirstPlayerController()->GetViewportSize(WidthOfViewport, HeightOfViewport);
	GetWorld()->GetFirstPlayerController()->ProjectWorldLocationToScreen(UOkpaLibrary::GetOkpaGameMode(this)->GetBaseLocationOfMainCatapultTrigger(), CurrentTutorial.InitialPositionOfFinger, true);

	CurrentTutorial.FinalPositionOfFinger.X = CurrentTutorial.InitialPositionOfFinger.X + (HeightOfViewport / 8);
	CurrentTutorial.FinalPositionOfFinger.Y = CurrentTutorial.InitialPositionOfFinger.Y + (HeightOfViewport / 8);

	CurrentTutorial.PositionOfText.X = WidthOfViewport / 2;
	CurrentTutorial.PositionOfText.Y = HeightOfViewport / 2;

	CurrentTutorial.TransitionToState(EOkpaTutorialState::INITIAL_STATE);

#if PLATFORM_ANDROID || PLATFORM_IOS
	FingerTexture = LoadObject<UTexture2D>(nullptr, *UOkpaLibrary::GetReferencePathOfAsset(TEXT("FingerImage")));
#else
	FingerTexture = LoadObject<UTexture2D>(nullptr, *UOkpaLibrary::GetReferencePathOfAsset(TEXT("MouseCursorImage")));
#endif

	FingerPressTexture = LoadObject<UTexture2D>(nullptr, *UOkpaLibrary::GetReferencePathOfAsset(TEXT("VisualClueThatRepresentsClick")));
	CurrentTutorialShouldBeDisplayed = true;
}


void AOkpaHUD::StopDisplayingTutorial()
{
	CurrentTutorialShouldBeDisplayed = false;
	FingerTexture = nullptr;
	FingerPressTexture = nullptr;
}


bool AOkpaHUD::TutorialIsBeingDisplayed() const
{
	return CurrentTutorialShouldBeDisplayed == true;
}


void AOkpaHUD::DisplayProjectilePath(const TArray<FVector>& ProjectilePathThatShouldBeDisplayed)
{
	APlayerController* PlayerController = GetWorld()->GetFirstPlayerController();

	CurrentProjectilePath.Empty(ProjectilePathThatShouldBeDisplayed.Num());
	CurrentProjectilePath.AddDefaulted(ProjectilePathThatShouldBeDisplayed.Num());

	for (int i = 0; i < ProjectilePathThatShouldBeDisplayed.Num(); ++i) {
		PlayerController->ProjectWorldLocationToScreen(ProjectilePathThatShouldBeDisplayed[i], CurrentProjectilePath[i], true);
	}
}


void AOkpaHUD::StopDisplayingCurrentProjectilePath()
{
	CurrentProjectilePath.Empty();
}


void AOkpaHUD::DisplayMagneticField(const AMagnet* MagnetWhoseFieldShouldBeDisplayed)
{
	if (CurrentMagnets.Find(MagnetWhoseFieldShouldBeDisplayed) == INDEX_NONE) {
		CurrentMagnets.Add(MagnetWhoseFieldShouldBeDisplayed);
	}

	if (MagneticFieldEdgePointTexture == nullptr) {
		MagneticFieldEdgePointTexture = LoadObject<UTexture2D>(nullptr, *UOkpaLibrary::GetReferencePathOfAsset(TEXT("MagneticFieldEdgePointImage")));
	}
}


void AOkpaHUD::StopDisplayingMagneticField(const class AMagnet* MagnetWhoseFieldShouldNoLongerBeDisplayed)
{
	CurrentMagnets.Remove(MagnetWhoseFieldShouldNoLongerBeDisplayed);
	
	if (CurrentMagnets.Num() == 0) {
		MagneticFieldEdgePointTexture = nullptr;
	}
}


void AOkpaHUD::DisplayCallout(const FString& CalloutTextValue, const FVector& CalloutLocation)
{
	FDataAboutCallout& CalloutToStartDisplaying = CurrentCallouts.AddDefaulted_GetRef();
	CalloutToStartDisplaying.Text = UOkpaLibrary::BreakIntoSeveralLines(CalloutTextValue);

	if (GetWorld() && GetWorld()->GetFirstPlayerController()) {
		GetWorld()->GetFirstPlayerController()->ProjectWorldLocationToScreen(CalloutLocation, CalloutToStartDisplaying.Position, true);
	}
}


void AOkpaHUD::StopDisplayingCurrentCallouts()
{
	CurrentCallouts.Empty();
}


void AOkpaHUD::DisplayFadeoutText(const FString& FadeoutTextValue, const FVector& FadeoutTextLocation)
{
	AFadeoutText* FadeoutText = GetWorld()->SpawnActor<AFadeoutText>(FadeoutTextLocation, FRotator(0, 90, 0));

	if (FadeoutText != nullptr) {
		FadeoutText->GetTextRender()->SetText(FText::FromString(FadeoutTextValue));
	}
}


void AOkpaHUD::StopDisplayingCurrentFadeoutTexts()
{
	UOkpaLibrary::DestroyAllActorsOfClass(AFadeoutText::StaticClass(), this);
}


void AOkpaHUD::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
	UpdateCurrentTutorial(DeltaSeconds);
	UpdateCurrentCallouts(DeltaSeconds);
	UpdateCurrentMagneticFields(DeltaSeconds);
}


void AOkpaHUD::UpdateCurrentTutorial(float TimeElapsedSinceLastUpdate)
{
	if (!CurrentTutorialShouldBeDisplayed) {
		return;
	}

	CurrentTutorial.TimeSpentInCurrentState += TimeElapsedSinceLastUpdate;

	switch (CurrentTutorial.CurrentState) {
	case INITIAL_STATE:
		if (CurrentTutorial.TimeSpentInCurrentState >= TIME_TO_SPEND_IN_INITIAL_STATE_OF_TUTORIAL) {
			CurrentTutorial.TransitionToState(GRIP_STATE);
		}

		break;

	case GRIP_STATE:
		if (CurrentTutorial.TimeSpentInCurrentState >= TIME_TO_SPEND_IN_GRIPPED_STATE_OF_TUTORIAL) {
			CurrentTutorial.TransitionToState(PULL_STATE);
		}

		break;

	case PULL_STATE:
		CurrentTutorial.CurrentPositionOfFinger += RESULTANT_VELOCITY_OF_TUTORIAL_FINGER * TimeElapsedSinceLastUpdate * (CurrentTutorial.FinalPositionOfFinger - CurrentTutorial.InitialPositionOfFinger) / FVector2D::Distance(CurrentTutorial.FinalPositionOfFinger, CurrentTutorial.InitialPositionOfFinger);

		if (CurrentTutorialFingerHasArrivedAtItsFinalPosition) {
			CurrentTutorial.TransitionToState(UNGRIP_STATE);
		}

		break;

	case UNGRIP_STATE:
		if (CurrentTutorial.TimeSpentInCurrentState >= TIME_TO_SPEND_IN_UNGRIPPED_STATE_OF_TUTORIAL) {
			CurrentTutorial.TransitionToState(INITIAL_STATE);
		}

		break;
	}
}


void AOkpaHUD::UpdateCurrentCallouts(float TimeElapsedSinceLastUpdate)
{
	int Index;
	int CountOfCurrentCallouts = CurrentCallouts.Num();

	for (Index = 0; Index < CountOfCurrentCallouts; ++Index) {
		CurrentCallouts[Index].Age += TimeElapsedSinceLastUpdate;
	}

	Index = 0;

	while (Index < CountOfCurrentCallouts) {
		if (CurrentCallouts[Index].Age >= MAXIMUM_AGE_OF_CALLOUT) {
			CurrentCallouts.RemoveAt(Index);
			CountOfCurrentCallouts = CurrentCallouts.Num();
			Index = 0;
		}
		else {
			++Index;
		}
	}
}

void AOkpaHUD::UpdateCurrentMagneticFields(float TimeElapsedSinceLastUpdate)
{
	CurrentRotationOfMagneticFieldRender -= SPEED_OF_ROTATION_OF_MAGNETIC_FIELD_RENDER * TimeElapsedSinceLastUpdate;
}


void AOkpaHUD::DrawHUD()
{
	Super::DrawHUD();
	DrawCurrentProjectilePath();
	DrawCurrentMagneticFields();
	DrawCurrentCallouts();
	DrawCurrentTutorial();
}


void AOkpaHUD::DrawCurrentProjectilePath()
{
	int LengthOfCurrentProjectilePath = CurrentProjectilePath.Num();

	if (LengthOfCurrentProjectilePath == 0 || ProjectilePointTexture == nullptr) {
		return;
	}

	FCanvasTileItem ProjectilePoint(FVector2D::ZeroVector, ProjectilePointTexture->Resource, FLinearColor::White);
	ProjectilePoint.BlendMode = SE_BLEND_Translucent;

	for (int i = 0; i < LengthOfCurrentProjectilePath; ++i) {
		ProjectilePoint.Position = CurrentProjectilePath[i];
		Canvas->DrawItem(ProjectilePoint);
	}
}


void AOkpaHUD::DrawCurrentMagneticFields()
{
	if (MagneticFieldEdgePointTexture == nullptr) {
		return;
	}

	FVector WorldLocation;
	FVector2D CenterOfMagneticField, EdgeOfMagneticField;
	float RadiusOfMagneticField;
	FCanvasTileItem BoundaryMarkerOfMagneticField(FVector2D::ZeroVector, MagneticFieldEdgePointTexture->Resource, FLinearColor::White);

	for (int i = 0; i < CurrentMagnets.Num(); ++i) {
		if (IsValid(CurrentMagnets[i])) {
			WorldLocation = CurrentMagnets[i]->GetActorLocation();
			GetWorld()->GetFirstPlayerController()->ProjectWorldLocationToScreen(WorldLocation, CenterOfMagneticField, true);

			WorldLocation.X += CurrentMagnets[i]->GetRadiusOfField();
			GetWorld()->GetFirstPlayerController()->ProjectWorldLocationToScreen(WorldLocation, EdgeOfMagneticField, true);

			RadiusOfMagneticField = FVector2D::Distance(CenterOfMagneticField, EdgeOfMagneticField);

			for (float Angle = CurrentRotationOfMagneticFieldRender; Angle < CurrentRotationOfMagneticFieldRender + (2 * PI); Angle += PI / 12) {
				EdgeOfMagneticField.X = CenterOfMagneticField.X + (RadiusOfMagneticField * FMath::Cos(Angle));
				EdgeOfMagneticField.Y = CenterOfMagneticField.Y + (RadiusOfMagneticField * FMath::Sin(Angle));
				BoundaryMarkerOfMagneticField.Position = EdgeOfMagneticField;
				BoundaryMarkerOfMagneticField.Rotation = FRotator(0, Angle * RADIANS_TO_DEGREES_CONVERSION_FACTOR, 0);
				Canvas->DrawItem(BoundaryMarkerOfMagneticField);
			}
		}
		else {
			StopDisplayingMagneticField(CurrentMagnets[i]);
		}

	}
}


void AOkpaHUD::DrawCurrentCallouts()
{
	int CountOfCurrentCallouts = CurrentCallouts.Num();

	if (CountOfCurrentCallouts == 0 || CalloutBackgroundTexture == nullptr) {
		return;
	}

	float ScaleOfCallout;
	float StableVerticalSizeOfCallout = CalloutBackgroundTexture->GetSizeY();

	int WidthOfViewport, HeightOfViewport;

	GetWorld()->GetFirstPlayerController()->GetViewportSize(WidthOfViewport, HeightOfViewport);


	for (int i = 0; i < CountOfCurrentCallouts; ++i) {
		if (CurrentCallouts[i].Age <= AGE_AT_WHICH_CALLOUT_ACHIEVES_MAXIMUM_SCALE) {
			ScaleOfCallout = MAXIMUM_SCALE_OF_CALLOUT * CurrentCallouts[i].Age / AGE_AT_WHICH_CALLOUT_ACHIEVES_MAXIMUM_SCALE;
		}
		else if (CurrentCallouts[i].Age <= AGE_AT_WHICH_CALLOUT_ACHIEVES_STABLE_SCALE) {
			ScaleOfCallout = MAXIMUM_SCALE_OF_CALLOUT - ((MAXIMUM_SCALE_OF_CALLOUT - STABLE_SCALE_OF_CALLOUT) * (CurrentCallouts[i].Age - AGE_AT_WHICH_CALLOUT_ACHIEVES_MAXIMUM_SCALE) / (AGE_AT_WHICH_CALLOUT_ACHIEVES_STABLE_SCALE - AGE_AT_WHICH_CALLOUT_ACHIEVES_MAXIMUM_SCALE));
		}
		else {
			ScaleOfCallout = STABLE_SCALE_OF_CALLOUT;
		}

		DrawTextureSimple(CalloutBackgroundTexture, CurrentCallouts[i].Position.X, CurrentCallouts[i].Position.Y - (StableVerticalSizeOfCallout * ScaleOfCallout), ScaleOfCallout);

		if (ScaleOfCallout == STABLE_SCALE_OF_CALLOUT) {
			DrawText(CurrentCallouts[i].Text, FLinearColor::Black, CurrentCallouts[i].Position.X + LEFT_PADDING_OF_CALLOUT, CurrentCallouts[i].Position.Y - StableVerticalSizeOfCallout + TOP_PADDING_OF_CALLOUT);
		}
	}
}


void AOkpaHUD::DrawCurrentTutorial()
{
	if (!CurrentTutorialShouldBeDisplayed || FingerTexture == nullptr || FingerPressTexture == nullptr) {
		return;
	}

	if ((CurrentTutorial.CurrentState == GRIP_STATE && CurrentTutorial.TimeSpentInCurrentState > TIME_TO_SPEND_IN_GRIPPED_STATE_OF_TUTORIAL / 2) || CurrentTutorial.CurrentState == PULL_STATE || (CurrentTutorial.CurrentState == UNGRIP_STATE && CurrentTutorial.TimeSpentInCurrentState < TIME_TO_SPEND_IN_UNGRIPPED_STATE_OF_TUTORIAL / 3)) {
		DrawTextureSimple(FingerPressTexture, CurrentTutorial.CurrentPositionOfFinger.X - (FingerPressTexture->GetSizeX() / 2), CurrentTutorial.CurrentPositionOfFinger.Y - (FingerPressTexture->GetSizeY() / 2));
	}

	if (CurrentTutorial.CurrentState != INITIAL_STATE) {
		DrawTextureSimple(FingerTexture, CurrentTutorial.CurrentPositionOfFinger.X, CurrentTutorial.CurrentPositionOfFinger.Y);
	}

	DrawText(CurrentTutorial.CurrentText, FLinearColor::White, CurrentTutorial.PositionOfText.X, CurrentTutorial.PositionOfText.Y, true, true, FLinearColor::Black);
}


void AOkpaHUD::DrawText(const FString& Text, FLinearColor TextColor, float ScreenX, float ScreenY, bool CentreAlign, bool ShadowShouldBeEnabled, FLinearColor ShadowColor, UFont* Font, float Scale, bool bScalePosition)
{
	if (IsCanvasValid_WarnIfNot()) {
		if (bScalePosition)
		{
			ScreenX *= Scale;
			ScreenY *= Scale;
		}

		FCanvasTextItem TextItem(FVector2D(ScreenX, ScreenY), FText::FromString(Text), Font == nullptr ? GEngine->GetMediumFont() : Font, TextColor);
		TextItem.Scale = FVector2D(Scale, Scale);
		TextItem.bCentreX = TextItem.bCentreY = CentreAlign;

		if (ShadowShouldBeEnabled) {
			TextItem.EnableShadow(ShadowColor);
		}

		Canvas->DrawItem(TextItem);
	}
}