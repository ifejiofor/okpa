// ActorThatCanMove.h: ActorThatCanMove class definition.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ActorThatCanMove.generated.h"


/**
 * ActorThatCanMove defines an actor that can move.
 * @author Ejiofor  Ifechukwu.
 */
UCLASS()
class OKPA_API AActorThatCanMove : public AActor
{
	GENERATED_BODY()
	
public:
	/** Sets default values for this actor's properties. */
	AActorThatCanMove();

	/** Starts moving this actor with a new velocity but without any specific destination. */
	void StartMovingWithNewVelocity(const FVector& NewVelocity, bool ShouldBeInfluencedByGravity = true);

	/** Starts moving this actor to a specific destination. */
	void StartMovingToSpecificDestination(const FVector& Destination, float TimeRequired = 2.0, bool ShouldBeInfluencedByGravity = false);

	/** Returns true if this actor is moving. Returns false otherwise. */
	bool IsMoving() const;

	/** Stops moving this actor. */
	void StopMoving();

	/** Returns the velocity of this actor. */
	virtual FVector GetVelocity() const override;

	/** Adds an additional velocity to the velocity of this actor. */
	void AddVelocity(const FVector& AdditionalVelocity);

	/** Returns true if this actor has arrived at its destination. */
	bool HasArrivedAtDestination() const;

	/** Enables sweep movement for this actor. Sweep movement causes this actor to collide, with other actors, while moving. */
	void EnableSweepMovement();

	/** Disables sweep movement for this actor.
	    @see EnableSweepMovement. */
	void DisableSweepMovement();

	/** Performs the processing that is to be performed periodically. */
	virtual void Tick(float DeltaTime) override;

private:
	/** Calculates and returns the displacement that this actor should cover within the specified time interval under the influence of gravity.
		@note This function also updates the Z-component of CurrentVelocity to reflect the influence of gravity on it. */
	FVector GetDisplacementUnderInfluenceOfGravity(float TimeInterval);

	/** Calculates and returns the linear displacement that this actor should cover within the specified time interval without the influence of gravity.
		@note This function does not update the CurrentVelocity. */
	FVector GetLinearDisplacement(float TimeInterval) const;

private:
	/** Velocity of this actor. */
	FVector CurrentVelocity;

	/** Destination where this actor is heading to.
		@note The value of this property is meaningful only when this actor is moving and DestinationArrivalIsRelevant is true. */
	FVector CurrentDestination;

	/** Flag that indicates whether or not this actor should stop moving whenever it arrives at its destination during movement. */
	bool DestinationArrivalIsRelevant;

	/** Flag that indicates whether or not this actor is being influenced by gravity during movement. */
	bool IsBeingInfluencedByGravity;

	/** Flag that indicates whether or not sweep movement is enabled for this actor.
		@see EnableSweepMovement. */
	bool SweepMovementStatus;
};